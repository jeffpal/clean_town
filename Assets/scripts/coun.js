﻿#pragma strict
private var myTimer : float = 300f; //tempo a ser decrementado (cinco minutos)
private var variv : int; // variavel que vai receber o tempo convertido de float para int

function FixedUpdate () {
	if (myTimer >0){ // se o tempo for maior que zero
		myTimer -= Time.deltaTime%60; //decremente-o em segundos e nao em frames per second
		variv = parseInt(myTimer); // a cada decremento, esta variavel recebera o valor
		
	if(myTimer <= 0){ // se o contador chegar a zero
		Application.LoadLevel("GameOver"); //a tela de game over sera exibida
		}
	}
}
function OnGUI(){ // funcao criada para alocar o cronometro regressivo na tela
	GUI.Label(Rect(Screen.width/16, Screen.height/6,255,255),"Tempo Restante: "+ variv.ToString());
}