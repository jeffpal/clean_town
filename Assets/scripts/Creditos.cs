﻿using UnityEngine;
using System.Collections;
//Script referente ao menu de Creditos
public class Creditos : MonoBehaviour {
	public Texture2D background; //Plano de fundo dos creditos
	private GUIStyle fonte; //Importa o pacote de fontes
	public float duracao; //variavel que recebera o tempo em segundos
	private float contagem; //contara o tempo de exibicao dos creditos
	
	
	void Start () {
		fonte = new GUIStyle();
		fonte.fontSize = 20; //tamanho da fonte que utilizamos
		fonte.normal.textColor = Color.white; //cor da fonte
	}
	
	// Update is called once per frame
	void Update () {
		contagem += Time.deltaTime; //fara a contagem do tempo
		if (contagem >= duracao) { // se o tempo contado for maior ou igual ao tempo que foi informado
			Application.LoadLevel(0); // o programa ira retornar para o nivel 0, que e o menu principal
		}
	}
	
	void OnGUI() { // Parametros do Rect: Rect(posição inicial de x, posição inicial de y, largura da imagem, altura da imagem)
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), background); //neste caso foi informado o plano de fundo dos creditos
		
		GUI.BeginGroup (new Rect(Screen.width/2, Screen.height/2, 256, 150));//Cria um grupo
		GUI.Label(new Rect(0, 0, 150, 20), "Debugger Game Studio: ", fonte); // Utiliza os parametros do Rect que foram informados anteriormente, mais uma string e a variavel fonte
		GUI.Label(new Rect(0, 30, 150, 20), "Jefferson Ferreira", fonte);
		GUI.Label(new Rect(0, 60, 150, 20), "Paulo Natan", fonte); // Utiliza os parametros do Rect que foram informados anteriormente, mais uma string e a variavel fonte
		GUI.Label(new Rect(0, 90, 150, 20), "Pedro Paulo", fonte); // Utiliza os parametros do Rect que foram informados anteriormente, mais uma string e a variavel fonte
		
		GUI.EndGroup(); //Encerra o grupo
	}
}