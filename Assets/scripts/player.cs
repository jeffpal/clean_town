﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class player : MonoBehaviour {
	private int count;
	public Text countText;
	public Text winText;
	private Animation mv;
	public AudioSource sound;

	void Start () {
		GetComponent<AudioSource> ();
		mv = GetComponent<Animation>();
		count = 0; //inicia o contador em 0
		SetCountText (); 
		winText.text = ""; //texto de vencedor iniciando vazio
	}
	
	// Update e chamado uma vez por frame
	void FixedUpdate () {

		//caso o comando "direita" seja executado entao executa-se as intruçoes 
		if(Input.GetButton("direita")){
			//esta funçao faz a rotaçao do objeto no eixo Y no sentido positivo
			transform.Rotate (0,60*Time.deltaTime,0);
			//essa instruçao executa a animaçao "Walk"
			mv.GetComponent<Animation>().CrossFade("Walk");
		}

		//caso o comando "esquerda" seja executado entao executa-se as intruçoes
		if(Input.GetButton("esquerda")){
			//esta funçao faz a rotaçao do objeto no eixo Y no sentido negativo
			transform.Rotate (0,-60*Time.deltaTime,0);
			//essa instruçao executa a animaçao "Walk"
			mv.GetComponent<Animation>().CrossFade("Walk");
		}

		//caso o comando "frente" seja executado entao executa-se as intruçoes
		if(Input.GetButton("frente")){
			//esta funçao faz a translaçao do objeto no eixo Z no sentido positivo
			transform.Translate (0,0,6*Time.deltaTime);
			//essa instruçao executa a animaçao "Walk"
			mv.GetComponent<Animation>().CrossFade("Walk");
		}

		//caso o comando "tras" seja executado entao executa-se as intruçoes
		if(Input.GetButton("tras")){
			//esta funçao faz a translaçao do objeto no eixo Z no sentido negativo
			transform.Translate (0,0,-6*Time.deltaTime);
			//essa instruçao executa a animaçao "Walk"
			mv.GetComponent<Animation>().CrossFade("Walk");
		}

		//caso o comando "Jump" seja executado entao executa-se as intruçoes
		if(Input.GetButton("Jump")){
			//essa instruçao executa a animaçao "Attack"
			mv.GetComponent<Animation>().CrossFade("Attack");
			
		}
	}
	
	void OnTriggerEnter(Collider other) //se o personagem tocar o objeto lixo, este objeto ira sumir e aumentar o score do jogo
	{
		if (other.gameObject.CompareTag ("trashpile")) //tag que identifica o lixo, e assim o personagem possa capturar o objeto referente ao lixo
		{
			other.gameObject.SetActive (false);
			count = count + 1; //incrementa o placar
			SetCountText (); //faz a contagem dos objetos coletados
		}	
	}

	void SetCountText ()
	{
		countText.text = "Capturados: " + count.ToString ();
		if(count >= 1){ //executara o som referente a coleta de lixo, a partir do primeiro coletado
			GetComponent<AudioSource>().Play (); //executa o audio quando o objeto e capturado
			//essa instruçao executa a animaçao "Attack"
			mv.GetComponent<Animation>().CrossFade("Attack");
		}
		if (count >= 100)
		{
			winText.text = "Venceu!"; //mensagem de "venceu!" que aparecera brevemente abaixo do placar.
			Application.LoadLevel("win"); // o programa ira direcionar a tela de jogar novamente.
		}
	}
}