﻿using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;
	//Menu valido
public class menu : MonoBehaviour {
	public Texture2D background;
	private GUIStyle fonte;
	
	void Start () {
		fonte = new GUIStyle();
		fonte.fontSize = 20;
	}
	
	void OnGUI(){
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), background);// Parametros do Rect: Rect(posição inicial de x, posição inicial de y, largura da imagem, altura da imagem)
		//a linha acima insere o plano de fundo no menu de jogo.
		if (GUI.Button (new Rect (Screen.width/2, Screen.height/2, 256, 30), "Jogar")) { //com esse parametro vai ser pego a metade horizontal e a metade vertical da tela
			Application.LoadLevel(4); //ao pressionar o botao, iria iniciar o jogo de fato
		}
		if (GUI.Button (new Rect (Screen.width/2, Screen.height/2 + 30, 256, 30), "Sobre")) { //Botao de creditos do jogo
			Application.LoadLevel("Creditos"); //ao precionar, ira exibir os nomes dos autores do jogo
		}
		if (GUI.Button (new Rect (Screen.width/2 , Screen.height/2 + 60, 256, 30),"Sair")){ //botao sair ou usar
			Application.Quit();// botao que encerra o jogo
		}	
	}
}
