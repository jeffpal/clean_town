﻿using UnityEngine;
using System.Collections;

public class crono : MonoBehaviour {
	public float myTimer = 5.0f;
	
	void Start() {
		InvokeRepeating( "DecreaseTime", 1, 1 ); // Called every second
	}
	
	void DecreaseTime() {
		myTimer--;
	}
	
	void onGUI() {
		GUI.Label(new Rect(50,10,150,100), "myTimer = " + myTimer );
	}
}
