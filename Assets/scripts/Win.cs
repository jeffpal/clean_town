﻿using UnityEngine;
using System.Collections;

public class Win : MonoBehaviour {
	public Texture2D background;
	public Texture2D titulo;
	public float duracao;
	private float contagem;
	
	void OnGUI(){
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), background); // Parametros do Rect: Rect(posição inicial de x, posição inicial de y, largura da imagem, altura da imagem)
		GUI.DrawTexture (new Rect (Screen.width/2, Screen.height/2, 256, 30), titulo);
		//GUI.BeginGroup (new Rect (Screen.width/2, Screen.height/2, 120, 120)); //cria um grupo
		//GUI.Box (new Rect (0, 0, 120, 120), "Jogar Novamente?");
		
		if (GUI.Button (new Rect (Screen.width/2, Screen.height/2 + 30, 256, 30), "Sim")) {
			//if (GUI.Button (new Rect (Screen.width/2, Screen.height/2, 50, 20), "Sim")) { //botao "sim"
			Application.LoadLevel ("MiniGame"); //ao ser pressionado ira iniciar uma nova partida
		}
		if (GUI.Button (new Rect (Screen.width/2, Screen.height/2 + 60, 256, 30), "Nao")) {
			//if (GUI.Button (new Rect (Screen.width/2, Screen.height/2 + 30, 50, 20), "Nao")) { //botao "nao"
			Application.Quit (); //ao ser pressionado ira encerrar o jogo, fechando a aplicacao
		}
		//GUI.EndGroup ();
	}
	void Update () {
		contagem += Time.deltaTime; //fara a contagem do tempo
		if (contagem >= duracao) { // se o tempo contado for maior ou igual ao tempo que foi informado
			Application.LoadLevel(0); // o programa ira retornar para o nivel 0, que e o menu principal
		}
	}
}