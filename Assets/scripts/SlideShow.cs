using UnityEngine;
using System.Collections;

[RequireComponent (typeof (GUITexture))]
public class SlideShow : MonoBehaviour{
	
	public Texture2D[] slides = new Texture2D[1];
	public float changeTime = 10.0f;
	private int currentSlide = 0;
	private float timeSinceLast = 1.0f;
	
	void Start(){
		
		GetComponent<GUITexture>().texture = slides[currentSlide];
		//GetComponent<GUITexture>().pixelInset = new Rect(-slides[currentSlide].width/2, -slides[currentSlide].height/2, slides[currentSlide].width, slides[currentSlide].height);
		//GetComponent<GUITexture>().pixelInset = new Rect (0, 0, Screen.width, Screen.height);
		GetComponent<GUITexture>().pixelInset = new Rect (Screen.width/2, Screen.height/2, 0,0);//centraliza a imagem de fundo de acordo com o tamanho da tela
		currentSlide++;
	}
	
	void Update(){
		
		if(timeSinceLast > changeTime && currentSlide < slides.Length)
		{
			GetComponent<GUITexture>().texture = slides[currentSlide];
			//GetComponent<GUITexture>().pixelInset = new Rect(-slides[currentSlide].width/2, -slides[currentSlide].height/2, slides[currentSlide].width, slides[currentSlide].height);
			//GetComponent<GUITexture>().pixelInset = new Rect (0, 0, Screen.width, Screen.height);
			GetComponent<GUITexture>().pixelInset = new Rect (Screen.width/2, Screen.height/2, 0, 0); //centraliza a imagem de fundo de acordo com o tamanho da tela
			timeSinceLast = 0.0f;
			currentSlide++;
		}
		// comment out this section if you don't want the slide show to loop
		// -----------------------
		if(currentSlide == slides.Length) //se o slide atual for o ultimo
		{
			currentSlide = 0;
			Application.LoadLevel(1); //inicie o jogo
		}
		// ------------------------
		timeSinceLast += Time.deltaTime;// conta o tempo decorrido para cada slide
	}
}